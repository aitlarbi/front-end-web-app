import { Component, OnInit } from '@angular/core';
import { employeeService } from '../services/employees.service';
import { employees_data } from '../data/employees.data';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  public employee:employees_data = new employees_data("","",0);
  public list: employees_data[] = [];
  constructor(private service:employeeService) 
  { 

  }

  ngOnInit(): void {
    this.getEmployee();
    this.getAllEmployee();
    //this.putEmployee();
    this.deleteEmployee();
  }
  public getEmployee()
  
  {
    this.service.getEmployee("001").subscribe(
    data => 
    {
      this.employee = data;
    }
  );}

  public getAllEmployee()
  
  {
    this.service.getAllEmployee().subscribe(
    data => 
    {
      this.list = data;
      for(let l in this.list){
        console.log("LST" +l);
      }
    }
  );}

public putEmployee()
  
  {
    let employee = new employees_data("004", "MEHDI ATAY", 1000)
    this.service.putEmployee(employee).subscribe();}


public deleteEmployee()
  
  {
    let id = "004";
    this.service.deleteEmployee(id).subscribe();
  }
}

