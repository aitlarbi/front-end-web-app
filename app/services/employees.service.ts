import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { employees_data } from "../data/employees.data";
import { map, Observable } from "rxjs";





@Injectable()
export class employeeService {
    private wsURL: string;

    private httpOption = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        })
    }
    constructor(private http: HttpClient){
        this.wsURL = "";
    };
    
    public getEmployee(id:string):Observable <employees_data>{
        console.log("zebi");
        return this.http.get("https://9kwjm8tv79.execute-api.us-east-2.amazonaws.com/employees/"+id, 
        this.httpOption).pipe(map((response: any) => {
            console.log(response)
            let employee = new employees_data("","",0);
            for(let l in response["Item"]) { 
                console.log(l)
               if (l=="id"){
                   employee.id=response["Item"][l];
                   console.log("id"+employee.id);
               }
               if (l=="name"){
                employee.name=response["Item"][l];
                console.log("name"+employee.name);
            }
                if (l=="Salary"){
                employee.salary=response["Item"][l];
                console.log("salary"+employee.salary);
            }
        }
            return employee;
        }

        ))};
        public getAllEmployee():Observable <employees_data[]>{
            console.log("zebi");
            return this.http.get("https://9kwjm8tv79.execute-api.us-east-2.amazonaws.com/employees", 
            this.httpOption).pipe(map((response: any) => {
                console.log(response)
                let employeeList = [];
                for(let l in response["Items"]) { 
                    let employee = new employees_data("","",0);
                    for(let i in response["Items"][l]){

                        if (i=="id"){
                            employee.id=response["Items"][l][i];
                            console.log("id"+employee.id);
                        }
                        if (i=="name"){
                         employee.name=response["Items"][l][i];
                         console.log("name"+employee.name);
                     }
                         if (i=="Salary"){
                         employee.salary=response["Items"][l][i];
                         console.log("salary"+employee.salary);
                     }
                    }
                   
                employeeList.push(employee);
            }
                return employeeList;
            }
            
            ))};
    public putEmployee(empl: employees_data):Observable<any> {
                console.log("zebi");
                return this.http.put("https://9kwjm8tv79.execute-api.us-east-2.amazonaws.com/employees", empl, 
                this.httpOption).pipe();
                }
                public deleteEmployee(id: string):Observable<any> {
                    console.log("zebi");
                    return this.http.delete("https://9kwjm8tv79.execute-api.us-east-2.amazonaws.com/employees/"+id, 
                    this.httpOption).pipe()};
    }

   
